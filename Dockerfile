FROM node:16-alpine as builder
WORKDIR '/app'
COPY package.json .
RUN npm install -g npm@8.5.1
RUN mkdir -p node_modules/.cache && chmod -R 777 node_modules/.cache
RUN npm install -g create-react-app
RUN npm install --save react react-dom
COPY . .
RUN npm run build

FROM nginx
EXPOSE 80 
#Causa 504 Gateway Time-out
COPY --from=builder /app/build /usr/share/nginx/html
